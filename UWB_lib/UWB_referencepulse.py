import numpy as np
# from numpy import sin, cos, exp, sqrt, divide
from sympy.abc import t, f
from sympy import sin, cos, exp, sqrt, pi, I, Piecewise, lambdify, re, im, limit, log, N, nan
from sympy.plotting import plot

from sympy.codegen.cfunctions import log10

# UWB signal function domain, either time or frequency
UWB_s_d = {
    "time": 0,
    "frequency": 1
}

# UWB signal frequency position, either signal on baseband or on passband
UWB_s_f = {
    "baseband": 0,
    "passband": 1
}


class Pulse:
    fourier_transformed_signal = None
    freq_shifted_signal = None

    def __init__(self, function, symbol, plot_start: float, plot_end: float, center_frequency: float, domain: str,
                 frequency_bb_pb: str, sampling_frequency: float, sampling_start: float, sampling_end: float,
                 plot_title: str = None):
        self.function = function
        self.symbol = symbol
        self.width = plot_end - plot_start
        self.start = plot_start
        self.end = plot_end
        self.center_frequency = center_frequency
        self.domain = domain
        self.frequency_bb_pb = frequency_bb_pb
        self.plot_title = plot_title
        self.sampling_start = sampling_start
        self.sampling_end = sampling_end
        self.sampling_frequency = sampling_frequency

        self.sampled_signal, self.t_arr = self.sample(sampling_start, sampling_end, sampling_frequency)

    def plot(self, show_real=True):
        func = re(self.function) if show_real else im(self.function)
        title = self.plot_title or "UWB reference pulse at %s, in %s domain" % (self.frequency_bb_pb, self.domain)
        x_label = "t: [ns]" if self.domain == "time" else "f: [GHz]"
        y_label = "r(t)" if self.domain == "time" else "H(f)"
        plt = plot(func,
                   (self.symbol, self.start, self.end),
                   title=title,
                   xlabel=x_label,
                   ylabel=y_label)
        return plt

    def sample(self, start=None, end=None, sampling_freq=None, sampling_period=None):
        start = start or self.sampling_start
        end = end or self.sampling_end
        sampling_freq = sampling_freq or self.sampling_frequency

        lambda_array = lambdify(self.symbol, self.function)
        if sampling_period:
            np_arr = np.arange(start, end, sampling_period)
        else:
            sampling_number = int(((end - start) * sampling_freq) + 0.5)
            np_arr = np.linspace(start, end, sampling_number)

        sampled_arr = lambda_array(np_arr)

        if 0 in np_arr:
            zero = N(self.function.subs(self.symbol, 0))
            if zero == nan:
                zero = N(limit(self.function, self.symbol, 0))
            zero = float(zero)

            sampled_arr = np.nan_to_num(sampled_arr, nan=zero)

        return sampled_arr, np_arr


class UWB_Pulse:
    t = t
    f = f
    reference_pulse = [[]]

    def __init__(self, T_pulse: float = 2.0, T_width: float = 0.5, f_center: float = 3993.6, oversampling: float = 1,
                 samp_width: float = 8,
                 bandwidth: float = 499.2, beta: float = 0.5):
        """
        Creates a sample UWB pulse in baseband, as a root-raised cosine, then shifts it to the desired center frequency.
        All real UWB pulses should have a normalized cross-correlation main lobe >= 0.8, with a duration of at
        least T_width, to be compliant with the standard. (see IEEE Std 802.15.4-2011, section 14.4.5)
        All time units are given in ns, all frequency units in MHz. Internally they are converted to GHz
        (1 ns = 10^-9 s , 1 GHz = 10^9 1/s)
        :param T_pulse: Pulse duration in nanoseconds
        :param T_width: main lobe width in nanoseconds
        :param f_center:  center frequency in megahertz
        :param oversampling: Oversampling factor relative to Nyquist sampling rate of highest frequency (keep >=1)
        :param samp_width:  duration of sampling in nanoseconds
        :param bandwidth: bandwidth of pulse in megahertz
        :param beta: roll-off factor
        """
        self.bw = bandwidth / 1000  # GHz
        self.s_w = samp_width  # ns
        self.ovs = oversampling  # Relative to Nyquist sampling rate: f_sam = ((f_c + bw/2) *2)*ovs

        self.f_c = f_center / 1000  # GHz
        self.T_p = T_pulse  # ns
        self.T_w = T_width / 1000  # GHz
        self.b = beta  # no unit

        self.f_sam = (self.f_c + self.bw / 2) * 2 * self.ovs  # GHz
        self.f_sam += self.f_sam % 2
        self.T_sam = 1 / self.f_sam  # ns

        signal_ref_baseband, self.s_r_b_scaling = self._ref_signal_td()

        p_time_baseband = Pulse(
            function=signal_ref_baseband,
            symbol=t,
            plot_start=-self.s_w,
            plot_end=self.s_w,
            center_frequency=0,
            domain="time",
            frequency_bb_pb="baseband",
            # sampling at passband frequency to allow frequency shift without reconstruction,
            # by multiplying sampled signal elementwise with exp(j*2*pi*freq*t)
            sampling_frequency=self.f_sam,

            sampling_start=-self.s_w / 2,
            sampling_end=self.s_w / 2)

        signal_ref_baseband_ft = self._ref_signal_fd()
        p_freq_baseband = Pulse(
            function=signal_ref_baseband_ft,
            symbol=f,
            plot_start=-self.bw * 2,
            plot_end=self.bw * 2,
            center_frequency=0,
            domain="frequency",
            frequency_bb_pb="baseband",
            sampling_frequency=40,  # frequency domain, therefore sampling just to approximate function
            sampling_start=-self.bw * (1 + self.b),
            sampling_end=self.bw * (1 + self.b))

        signal_ref_passband_td = self._freq_shift_td(signal_ref_baseband)
        p_time_passband = Pulse(
            function=signal_ref_passband_td,
            symbol=t,
            plot_start=-self.s_w,
            plot_end=self.s_w,
            center_frequency=self.f_c,
            domain="time",
            frequency_bb_pb="passband",
            sampling_frequency=self.f_sam,
            sampling_start=-self.s_w / 2,
            sampling_end=self.s_w / 2)

        signal_ref_passband_ft = self._freq_shift_fd(signal_ref_baseband_ft)
        p_freq_passband = Pulse(  # only first positive shifted signal shown and sampled
            function=signal_ref_passband_ft,
            symbol=f,
            plot_start=self.f_c - self.bw * 2,
            plot_end=self.f_c + self.bw * 2,
            center_frequency=self.f_c,
            domain="frequency",
            frequency_bb_pb="passband",
            sampling_frequency=40,  # frequency domain, therefore sampling just to approximate function
            sampling_start=self.f_c - self.bw * (1 + self.b),
            sampling_end=self.f_c + self.bw * (1 + self.b))

        self.reference_pulse = [[p_time_baseband, p_time_passband],
                                [p_freq_baseband, p_freq_passband]]

        psd = self._ref_signal_power_spectral_density()
        psd_width = (1 + self.b) / (2 * self.T_p)
        self.reference_psd = Pulse(
            function=psd,
            symbol=f,
            plot_start=-(0.75 + self.b) / (2 * self.T_p),
            plot_end=(0.75 + self.b) / (2 * self.T_p),
            center_frequency=self.f_c,
            domain="frequency",
            frequency_bb_pb="baseband",
            sampling_frequency=60,  # frequency domain, therefore sampling just to approximate function
            sampling_start=-psd_width,
            sampling_end=psd_width,
            plot_title="Power Spectrum density in dB")

    def _ref_signal_td(self):
        s = self
        c = 4 * s.b / (pi * sqrt(s.T_p))
        arg = (1 + s.b) * pi * t / s.T_p
        sin_p = sin(arg) / (4 * s.b * t / s.T_p)

        nom = cos(arg) + sin_p
        den = 1 - (4 * s.b * t / s.T_p) ** 2

        func = nom / den * c
        return func, c

    def _ref_signal_fd(self):
        w0 = self.bw  # GHz
        w = w0 * (1 + self.b)  # GHz

        # center = 1       # abs(f) < 2*w0 - w
        left = cos(pi / 4 * (-f + w - 2 * w0) / (w - w0))  # - w <= f <= -(2*w0 - w)
        right = cos(pi / 4 * (f + w - 2 * w0) / (w - w0))  # (2*w0 - w) <= f <= w
        # over = 0    # abs(f) > w
        func = Piecewise(
            (0, f < -w),
            (left, f <= -(2 * w0 - w)),  # - w <= f <= -(2*w0 - w)
            (1, f < 2 * w0 - w),  # abs(f) < 2*w0 - w
            (right, f <= w),  # (2*w0 - w) <= f <= w
            (0, True))
        return func

    def _ref_signal_power_spectral_density(self):
        T_p = self.T_p
        b = self.b

        p_ext = (1 + b) / (2 * T_p)
        p_int = (1 - b) / (2 * T_p)

        p_band_right = T_p / 2 * (1 + cos(pi * T_p / b * (f - (1 - b) / (2 * T_p))))
        p_band_left = p_band_right.subs(f, -f)

        func = Piecewise(
            (0, f < -p_ext),
            (p_band_left, f < -p_int),
            (T_p, f <= p_int),
            (p_band_right, f <= p_ext),
            (0, True)
        )
        return 20 * log10(func)

    def _freq_shift_td(self, signal_td):
        e_shift = exp(2 * pi * I * t * self.f_c)
        return signal_td * e_shift

    def _freq_shift_fd(self, signal_fd):
        f_shift = signal_fd.subs(f, f - self.f_c)
        return f_shift

    def plot_impulse(self, domain="time", frequency_bb_pb="baseband"):
        self.reference_pulse[UWB_s_d[domain]][UWB_s_f[frequency_bb_pb]].plot()

    def get_sampled_impulse(self, domain="time", frequency_bb_pb="baseband"):
        signal = self.reference_pulse[UWB_s_d[domain]][UWB_s_f[frequency_bb_pb]]
        return signal.sampled_signal, signal.t_arr

    def get_impulse(self, domain="time", frequency_bb_pb="passband"):
        return self.reference_pulse[UWB_s_d[domain]][UWB_s_f[frequency_bb_pb]]

    def _sample(self, signal=None, domain="time", frequency_bb_pb="baseband"):
        signal = signal or self.signal_ref_baseband

        var = next(iter(signal.free_symbols))
        lambda_array = lambdify(var, signal)
        n_sampling = self.f_sam * self.s_w
        sam_width = self.s_w / 2
        np_arr = np.linspace(-sam_width, sam_width, int(n_sampling))
        sampled_arr = lambda_array(np_arr)
        return sampled_arr
