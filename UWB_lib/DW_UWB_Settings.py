DW_channels = {
    "1": 0,
    "2": 1,
    "3": 2,
    "4": 3,
    "5": 4,
    "7": 5
}

DW_BR = {  # Decawave Bitrate values
    "0.11": 0,
    "0.85": 1,
    "6.81": 2
}
DW_MPRF = {  # Decawave Mean Pulse Repetition Frequency values
    "16": 0,
    "64": 1
}
DW_NSYNC = {
    "16": 16,
    "64": 64,
    "1024": 1024,
    "4069": 4096
}
DW_NSFD = {  # Decawave Symbol Frame Duration
    "8": 8,
    "64": 64
}
DW_NCCA_PHR = {  # Number of multiplexed preamble symbols in PHR
    "4": 4,
    "32": 32
}
DW_UWB_Channels_BW = {
    "Channel_number": [1, 2, 3, 4, 5, 7],
    "Centre_freq": [3494.4, 3993.6, 4492.8, 3993.6, 6489.6, 6489.6],
    "Band_begin": [3244.8, 3774, 4243.2, 3328, 6240, 5980.3],
    "Band_end": [3744, 4243.2, 4742.4, 4659.2, 6739.2, 6998.9],
    "Bandwidth": [499.2] * 3 + [1331.2, 499.2, 1081.6]
}
DW_UWB_PHY_PSDU_timing = {  # Decawave UltraWideBand Physical layers settings
    # Values from Std 802.15.4-2011, Table 99
    "Peak_PRF": [499.2] * 6,  # [MHZ], Peak Pulse Repetition Frequency
    "BW_MHz": [499.2] * 6,  # [MHZ] , Bandwidth
    "PCL": [31] * 3 + [127] * 3,  # Preamble code length (see Table 102 & Table 103)
    "VR": [0.5] * 6,  # Viterbi Rate       (see 14.3.3.2)
    "RS_rate": [0.87] * 6,  # Reed solomon (63,55) code rate (see 14.3.3.1)
    "FEC_Rate": [0.44] * 6,  # Overall Forward Error Correction, product of RS and VR
    "N_burst": [32] * 3 + [8] * 3,  # Burst positions per symbol parameter, total number of possible burst positions in
    # data symbol duration. Fixed number for each mean PRF.
    "N_hop": [8] * 3 + [2] * 3,  # hop bursts parameter, number of burst positions that may contain an UWB pulse
    "N_cpb": [128, 16, 2, 512, 64, 8],  # Chips per burst, number of chip T_c durations within each T_burst (Figure 85)
    "N_cps": [4096, 512, 64] * 2,  # Chips per Symbol (N_burst*N_cpb)
    "T_burst": [256.41, 32.05, 4.01, 1025.64, 128.21, 16.03],  # [ns] burst duration
    "T_dsym": [8205.13, 1025.64, 128.1] * 2,  # [ns] Symbol duration, of a modulated, coded and sent PSDU symbol
    "Sym_rate": [0.12, 0.98, 7.8] * 2,  # [MHz] Symbol rate, = 1/T_dsym
    "Bit_Rate": [0.11, 0.85, 6.81] * 2,  # [MHz] user information rate considering FEC, 2*FEC_rate/T_dsym
    "Mean_PRF": [15.60] * 3 + [62.40] * 3  # [MHz] Mean Pulse Repetition Frequency
}

DW_UWB_PHY_SHR_timing = {
    "Code_length": [31, 127],
    "Peak_PRF": [31.2, 124.8],  # [MHz]
    "Mean_PRF": [16.1, 62.89],  # [MHz]
    "Delta_l": [16, 4],
    "N_cps": [496, 508],  # Chips per symbol
    "T_psym": [993.59, 1017.63],  # [ns]
    "Base_rate": [1.01, 0.98]  # [Msymbols/s]
}
UWB_ternary_codes = {
    "1": "-0000+0-0+++0+-000+-+++00-+0-00",  # Channel number 0,1,8,12
    "2": "0+0+-0+0+000-++0-+---00+00++000",  # Channel number 0,1,8,12
    "3": "-+0++000-+-++00++0+00-0000-0+0-",  # Channel number 2,5,9,13
    "4": "0000+-00-00-++++0+-+000+0-0++0-",  # Channel number 2,5,9,13
    "5": "-0+-00+++-+000-+0+++0-0+0000-00",  # Channel number 3,6,10,14
    "6": "++00+00---+-0++-000+0+0-+0+0000",  # Channel number 3,6,10,14
    "7": "+0000+-0+0+00+000+0++---0-+00-+",  # Channel number 4,7,11,15
    "8": "0+00-0-0++0000--+00-+0++-++0+00",  # Channel number 4,7,11,15
    # Channel number 0–4, 5, 6, 8–10, 12–14
    "9": "+00+000-0--00--+0+0+00-+-++0+0000++-000+00-00--0-+0+0--0-+++0++000+-0+00-0++-0+++00-+00+0+0-0++-+--+000000+00000-+0000-0-000--+",
    "10": "++00+0-+00+00+000000-000-00--000-0+-+0-0+-0-+00000+-00++0-0+00--+00++-+0+-0+0000-0-0-0-++-+0+00+0+000-+0+++000----+++0000+++0--",
    "11": "-+-0000+00--00000-0+0+0+-0+00+00+0-00-+++00+000-+0+0-0000+++++-+0+--0+-0++--0-000+0-+00+0+----000-000000-+00+-0++000++-00++-0-0",
    "12": "-+0++000000-0+0-+0---+-++00-+0++0+0+0+000-00-00-+00+-++000-+-0-++0-0++++0-00-0++00+0+00++-00+000+-000-0--+0000-0000--0+00000+--",
    # Channel number 0–15; DPS only
    "13": "+000--0000--++0-++++0-0++0+0-00-+0++00++-0++0+-+0-00+00-0--000-+-00+0000-0++-00000+-0-000000-00-+-++-+000-0+0+0+++-00--00+0+000",
    "14": "+000++0-0+0-00+-0-+0-00+0+0000+0+-0000++00+0+++++-+0-0+-0--+0++--000---0+000+0+0-+-000000+-+-0--00++000-00+00++-00--++-00-00000",
    "15": "0+-00+0-000-++0000---++000+0+-0-+00-+000--0-00--0--+++-+0-++00+-++0+00000+0-0+++-00+00+000-0000+00--+0++0+0+0-00-0-+-0+0++00000",
    "16": "++0000+000+00+--0+-++0-000--00+-0+00++000+++00+0+0-0-+-0-0+00+00+0++----+00++--+0+-0--+000000-0-0000-+0--00+00000+-++000-0-+0+0",
    # Channel number 4, 7, 11, 15
    "17": "+--000-0-0000+-00000+000000+--+-++0-0+0+00+-00+++0-++0-00+0-+000++0+++-0--0+0+-0--00-00+000-++0000+0++-+-00+0+0+--00--0-000+00+",
    "18": "--0+++0000+++----000+++0+-000+0+00+0+-++-0-0-0-0000+0-+0+-++00+--00+0-0++00-+00000+-0-+0-0+-+0-000--00-000-000000+00+00+-0+00++",
    "19": "-0-++00-++000++0-+00+-000000-000----+0+00+-0+000-0--++0-+0--+0+-+++++0000-0+0+-000+00+++-00-0+00+00+0-+0+0+0-00000--00+0000-+-0",
    "20": "--+00000+0--0000-0000+--0-000-+000+00-++00+0+00++0-00-0++++0-0++-0-+-000++-+00+-00-00-000+0+0+0++0+-00++-+---0+-0+0-000000++0+-",
    # Channel number 0–15; DPS only
    "21": "+0+00--00-+++0+0+0-000+-++-+-00-000000-0-+00000-++0-0000+00-+-000--0-00+00-0+-+0++0-++00++0+-00-0+0++0-0++++-0++--0000--000+000",
    "22": "0-00-++--00-++00+00-000++00--0-+-+000000-+-0+0+000+0---000--++0+--0-+0-0+-+++++0+00++0000-+0+0000+0+00-0+-0-+00-0+0-0++000+0000",
    "23": "000++0+0-+-0-00-0+0+0++0+--00+0000-000+00+00-+++0-0+00000+0++-+00++-0+-+++--0--00-0--000+-00+-0-+0+000++---0000++-000-0+00-+000",
    "24": "+0+-0-000++-+00000+00--0+-0000-0-000000+--0-+0+--++00+----++0+00+00+0-0-+-0-0+0+00+++000++00+0-+00--000-0++-+0--+00+000+0000++0"

}
UWB_SFD_ternary_codes = {
    "8": [0, 1, 0, -1, 1, 0, 0, -1],
    "64": [0, 1, 0, -1, 1, 0, 0, -1, 0, 1, 0, -1, 1, 0, 0, -1, -1, 0, 0, 1, 0, -1, 0, 1, 0, 1, 0, 0, 0, -1, 0, -1, 0,
           -1, 0, 0, 1, 0, -1, -1, 0, -1, 1, 0, 0, 0, 0, 1, 1, 0, 0, -1, -1, -1, 1, -1, 1, 1, 0, 0, 0, 0, 1, 1]
}
UWB_PHR_SyncCodes = {
    "16": [0, 0],
    "64": [0, 1],
    "1024": [1, 0],
    "4069": [1, 1]
}
UWB_PHR_DR_codes = {
    "0.11": [0, 0],
    "0.85": [0, 1],
    "6.81": [1, 0]
}


def uwb_compute_shr_timings(shr_timings, n_sfd, n_sync, n_cca_phr, pdsu_timings, channel_code):
    shr_timings["N_sfd"] = DW_NSFD[n_sfd]
    shr_timings["T_sfd"] = shr_timings["N_sfd"] * shr_timings["T_psym"]
    shr_timings["N_sync"] = DW_NSYNC[n_sync]
    shr_timings["T_sync"] = shr_timings["N_sync"] * shr_timings["T_psym"]
    shr_timings["N_pre"] = shr_timings["N_sync"] + shr_timings["N_sfd"]
    shr_timings["T_pre"] = shr_timings["T_sfd"] + shr_timings["T_sync"]
    shr_timings["N_cca_phr"] = DW_NCCA_PHR[n_cca_phr]
    shr_timings["N_cca_data"] = shr_timings["N_pre"] / (4 * pdsu_timings["T_dsym"])
    shr_timings["T_per_chip"] = shr_timings["T_psym"] * shr_timings["Delta_l"] / shr_timings["N_cps"]
    ternary_code = UWB_ternary_codes[channel_code]
    assert len(ternary_code) == shr_timings["Code_length"]
    shr_timings["ternary_code"] = ternary_code
    shr_timings["SFD_ternary_code"] = UWB_SFD_ternary_codes[n_sfd]

    return shr_timings


def uwb_xor(array_to_xor, positions):
    out = array_to_xor[positions[0]]
    for pos in positions[1:]:
        out ^= array_to_xor[pos]
    return out


def uwb_compute_phr(frame_length, ranging_packet, n_sync, bitrate):
    arr_fl = [(frame_length >> (7 - i)) & 1 for i in range(7)]
    arr_pd = UWB_PHR_SyncCodes[n_sync]
    arr_rp = [int(ranging_packet)]
    arr_br = UWB_PHR_DR_codes[bitrate]
    pirate = arr_br + arr_fl + arr_rp + [0] + arr_pd
    c0 = uwb_xor(pirate, [1, 0, 8, 6, 4, 3, 10, 11])
    c1 = uwb_xor(pirate, [0, 6, 5, 3, 2, 9, 10, 12])
    c2 = uwb_xor(pirate, [1, 8, 7, 3, 2, 9, 10])
    c3 = uwb_xor(pirate, [8, 7, 6, 5, 4, 9, 10])
    c4 = uwb_xor(pirate, [12, 11])
    phr_header = pirate + [0, c4, c3, c2, c1, c0]
    c5 = uwb_xor(phr_header, list(range(13))+list(range(14, 18)))
    phr_header[13] = c5
    return phr_header


def uwb_settings_dict(channel: str = "3",
                      bitrate: str = "6.81",
                      mean_prf: str = "16",
                      n_sfd: str = "8",
                      n_sync: str = "64",
                      n_cca_phr: str = "4",
                      channel_code: str = "9",
                      frame_length: int = 127,
                      ranging_packet: bool = True):
    """

    :param channel:     specified in DW_channels
    :param bitrate:     specified in DW_BR
    :param mean_prf:    mean pulse repetition frequency, specified in DW_MPRF
    :param n_sfd:       number of symbols in sfd, specified in DW_NSFD
    :param n_sync:      number of symbols in packet sync sequence, specified in DW_NSYNC
    :param n_cca_phr:   number of multiplexed preamble symbols in PHR
    :param channel_code: specified in UWB_ternary_codes, consider comments to choose right channel
    :param frame_length: 0-127, Dataframe length in Bytes. final length in symbols: 8*frame_lenth + Reed_solomon bits
    :param ranging_packet: indicates if current packet is an RFRAME

    :return: channel characteristics, PHY service data unit and Synchronisation header timing constants
    """
    m_prf_code = DW_MPRF[mean_prf]
    channel_n = DW_channels[channel]
    array_address = DW_BR[bitrate] + 3 * m_prf_code

    pdsu_timings = {key: DW_UWB_PHY_PSDU_timing[key][array_address] for key in DW_UWB_PHY_PSDU_timing}

    channel_band = {key: DW_UWB_Channels_BW[key][channel_n] for key in DW_UWB_Channels_BW}

    shr_timings = {key: DW_UWB_PHY_SHR_timing[key][m_prf_code] for key in DW_UWB_PHY_SHR_timing}
    shr_timings = uwb_compute_shr_timings(shr_timings, n_sfd, n_sync, n_cca_phr, pdsu_timings, channel_code)

    pdsu_timings["PHR_header"] = uwb_compute_phr(frame_length, ranging_packet, n_sync, bitrate)

    return channel_band, pdsu_timings, shr_timings
