import numpy as np
import matplotlib as mpl
from typing import Union
from .DW_UWB_Settings import uwb_settings_dict


class UWBSignal:

    def __init__(self, signal: Union[str, list],
                 channel: str = "3",
                 bitrate: str = "6.81",
                 mean_prf: str = "64",
                 n_sfd: str = "8",
                 n_sync: str = "64",
                 n_cca_phr: str = "4"):
        if isinstance(signal, str):
            self.signal = self.uwb_signal_parser(signal)
        elif isinstance(signal, list):
            self.signal = signal
        else:
            raise TypeError("Type of signal for WurSignal __init__ should be string or list, but is "
                            + str(type(signal)))
        self.channel_settings, self.pdsu_timings, self.shr_timings = \
            uwb_settings_dict(channel, bitrate, mean_prf, n_sfd, n_sync, n_cca_phr)
        self.shr_timings["ternary_code"] = self.uwb_signal_parser(self.shr_timings["ternary_code"])
        self.start_time = 0.0

    @staticmethod
    def uwb_signal_parser(signal: str) -> list:
        """
        Parses a WUR signal string transforming it to a list

        :param signal: string composed of (+, -, 0)
        :return: list of (1, -1, 0) respectively
        """
        signal = signal.strip()
        return [int((i == '+') - (i == '-')) for i in signal]

    def uwb_shr_sync(self, start_time_ns: float = 0.0) -> list:
        """
        Takes a WUR signal and configuration data and converts
        it to a list of tuples containing the impulse magnitude
        and the impulse time during the preamble. timings starting from 0
        :return:
        """
        symbol_pattern = self.shr_timings["ternary_code"]

        delta_l = self.shr_timings["Delta_l"]
        n_sync = self.shr_timings["N_sync"]
        n_cps = self.shr_timings["N_cps"]
        t_psym = self.shr_timings["T_psym"]
        t_per_chip = self.shr_timings["T_per_chip"]
        # time_chip_array = [symbol_pattern[i // delta_l] * (not(i % delta_l)) for i in range(code_length * delta_l)]
        time_chip_array = [(val, start_time_ns + t_per_chip * i + j * t_psym)
                           for j in range(n_sync)
                           for i, val in enumerate(symbol_pattern)]
        self.shr_timings["symbol_time_chip_array"] = time_chip_array
        self.start_time = time_chip_array[-1][1]
        return time_chip_array

    def uwb_shr_sfd(self, start_time_ns: float = 0.0):
        """
        Generates the SFD (Start Frame Delimiter) signal for the UWB signal.

        Args:
            start_time_ns (float): The start time of the signal in nanoseconds. Defaults to 0.0.

        Returns:
            List[Tuple[float, float]]: A list of tuples representing the time and value of each chip in the SFD signal.
        """
        t_psym = self.shr_timings["T_psym"]
        t_per_chip = self.shr_timings["T_per_chip"]
        sfd_time_chip_array = [(symbol * val, start_time_ns + t_per_chip * i + j * t_psym)
                               for j, val in enumerate(self.shr_timings["SFD_ternary_code"])  # TODO what is wrong here
                               for i, symbol in enumerate(self.shr_timings["ternary_code"])]
        self.shr_timings["SFD_time_chip_array"] = sfd_time_chip_array
        # print(sum(sfd_time_chip_array))
        self.start_time = sfd_time_chip_array[-1][1]
        return sfd_time_chip_array

    def uwb_phr(self, start_time_ns: float = 0.0):
        """
        Generates the PHR (Pulse Header) signal for UWB communication.

        Args:
            start_time_ns (float): The start time of the PHR signal in nanoseconds. Defaults to 0.0.

        Returns:
            list: A list of tuples containing the PHR signal and the corresponding time in nanoseconds.
        """
        t_psym = self.shr_timings["T_psym"]
        t_per_chip = self.shr_timings["T_per_chip"]
        phr_time_chip_array = [(symbol * val, start_time_ns + t_per_chip * i + j * t_psym)
                               for j, val in enumerate(self.pdsu_timings["PHR_header"])
                               for i, symbol in enumerate(self.shr_timings["ternary_code"])]
        self.shr_timings["PHR_time_chip_array"] = phr_time_chip_array
        self.start_time = phr_time_chip_array[-1][1]
        return phr_time_chip_array

    def output_preamble(self):
        """
        Returns the preamble signal for the UWB signal.
        The preamble consists of the short-range sync, short-range SFD, and the long-range Preamble Header.

        Returns:
            list: A list of tuples containing the preamble signal and the corresponding time in nanoseconds.
        """
        return self.uwb_shr_sync(self.start_time) + self.uwb_shr_sfd(self.start_time) + self.uwb_phr(self.start_time)

    def uwb_signal(self, start_time_ns: float = 0.0):
        """
        Generates a UWB signal based on the given signal and shared timings.

        Args:
        start_time_ns (float): The start time of the signal in nanoseconds. Defaults to 0.0.

        Returns:
        List[Tuple[int, float]]: A list of tuples containing the chip value and the time of transmission.
        """
        # TODO: add signal coding algorithm
        t_psym = self.shr_timings["T_psym"]
        t_per_chip = self.shr_timings["T_per_chip"]
        signal_time_chip_array = [(symbol * val, start_time_ns + t_per_chip * i + j * t_psym)
                                  for j, val in enumerate(self.signal)
                                  for i, symbol in enumerate(self.shr_timings["ternary_code"])]
        self.shr_timings["UWB_signal"] = signal_time_chip_array
        self.start_time = signal_time_chip_array[-1][1]
        return signal_time_chip_array

    def output_signal(self):
        self.start_time = 0
        return self.output_preamble() + self.uwb_signal(self.start_time)

#  def something(self, unsi: object, deine: object) -> object:
